package com.spark.match.repo;

import java.util.List;

import com.spark.match.model.FilterCriteria;
import com.spark.match.model.Match;

public interface MatchRepositoryCustom {

	List<Match> getMatches(FilterCriteria filterCriteria);
}
