package com.spark.match.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.spark.match.model.Match;

@Repository
public interface MatchRepository extends JpaRepository<Match, Long>, MatchRepositoryCustom{

}
