package com.spark.match.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.spark.match.exception.EntityNotFoundException;
import com.spark.match.model.FilterCriteria;
import com.spark.match.model.Match;
import com.spark.match.service.MatchService;

@RestController
@RequestMapping("/match")
@CrossOrigin
public class MatchFilterController {

	@Autowired
	private MatchService matchService;

	@PostMapping(value = "/initialize-db")
	public List<Match> initializeDb() {
		return matchService.initializeMatchData();
	}

	@PostMapping(value = "/filter", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody List<Match> getFilteredMatches(@RequestBody FilterCriteria filterCriteria) {
		return matchService.getMatches(filterCriteria);
	}

	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Match getMatchById(@PathVariable Long id) throws Exception {
		return matchService.getMatchById(id).map(m -> {
			return m;
		}).orElseThrow(() -> new EntityNotFoundException(Match.class, "id", id.toString()));
	}
	
	@PostMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE)
	public Match getFilteredMatches(@RequestBody Match match) {
		 return matchService.updateMatch(match);
	}
}
