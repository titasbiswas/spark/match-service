package com.spark.match.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spark.match.model.FilterCriteria;
import com.spark.match.model.Match;
import com.spark.match.repo.MatchRepository;
import com.spark.match.util.JsonDbUtils;

@Service
public class MatchServiceImpl implements MatchService{
	
	private final String JSON_SAMPLE_DATA_FILE_PATH = "/json/matches.json";

	@Autowired
	//JsonDbUtils<User, JpaRepository<User,Long>, Long> jsonDbUtils;
	private JsonDbUtils jsonDbUtils;
	
	@Autowired
	private MatchRepository matchRepository;
	
	public List<Match> initializeMatchData(){		
		return jsonDbUtils.insertJsonToMatchTable(JSON_SAMPLE_DATA_FILE_PATH);		
	}

	@Override
	public List<Match> getMatches(FilterCriteria filterCriteria) {
		return matchRepository.getMatches(filterCriteria);
	}
	
	@Override
	public Optional<Match> getMatchById(Long id) {
		return matchRepository.findById(id);
	}

	@Override
	public Match updateMatch(Match match) {
		return matchRepository.findById(match.getId()).map(m -> {
			if(match.getHidden() != null) {
				m.setHidden(match.getHidden());
			}
			if(match.getFavourite() != null) {
				m.setFavourite(match.getFavourite());
			}
			if(match.getContactsExchanged() != null) {
				if((match.getContactsExchanged() -  m.getContactsExchanged()) == 1)
				m.setContactsExchanged(match.getContactsExchanged());
			}
			return matchRepository.save(m);
		}).orElseThrow(() -> new RuntimeException());

	}
}
