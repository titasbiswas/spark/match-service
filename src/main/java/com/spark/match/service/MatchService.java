package com.spark.match.service;

import java.util.List;
import java.util.Optional;

import com.spark.match.model.FilterCriteria;
import com.spark.match.model.Match;

public interface MatchService {	
	List<Match> initializeMatchData();
	List<Match> getMatches(FilterCriteria filterCriteria);
	Optional<Match> getMatchById(Long id);
	Match updateMatch(Match match);	
}
