package com.spark.match.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name="matches")
public class Match {
	@Id
	@SequenceGenerator(name="match_seq_generator", sequenceName = "matches_id_seq", allocationSize=1, initialValue=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "match_seq_generator")	
	private Long id;
	
	@Column(name="display_name")
	private String displayName;	
	private Integer age;
	@Column(name="job_title")
	private String jobTitle;	
	@Column(name="height_in_cm")
	private Integer heightInCm;
	@Embedded
	private City city = new City();
	@Column(name="main_photo")
	private String mainPhoto;
	@Column(name="compatibility_score")
	private BigDecimal compatibilityScore;
	@Column(name="contacts_exchanged")
	private Integer	contactsExchanged;
    private Boolean favourite;
    private String religion;
    private Boolean hidden;

}
