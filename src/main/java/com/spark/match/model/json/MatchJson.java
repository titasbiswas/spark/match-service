package com.spark.match.model.json;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MatchJson {


	private String display_name;	
	private Integer age;
	private String job_title;	
	private Integer height_in_cm;
	private CityJson city = new CityJson();
	private String main_photo;
	private BigDecimal compatibility_score;
	private Integer	contacts_exchanged;
    private Boolean favourite;
    private String religion;

}
