package com.spark.match.model;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LocationDistance {

	private Double distanceInKm;
	private BigDecimal lat;
	private BigDecimal lon;

}
