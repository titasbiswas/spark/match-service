package com.spark.match.controller;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.spark.match.model.FilterCriteria;
import com.spark.match.model.Match;
import com.spark.match.service.MatchService;
import com.spark.match.test.util.DummyObjectFactory;

@RunWith(SpringRunner.class)
@WebMvcTest(MatchFilterController.class)
public class MatchFilterControllerTest {
	
	@Autowired
    private MockMvc mvc;
 
    @MockBean
    private MatchService matchService;
    
    @Test
    public void testGetFilteredMatches() throws Exception {
    	List<Match> dummyMatchList = new ArrayList<>();
		dummyMatchList.add((Match) DummyObjectFactory.populateDummyObject(new Match()));
		dummyMatchList.add((Match) DummyObjectFactory.populateDummyObject(new Match()));
		
		when(matchService.getMatches(Mockito.any(FilterCriteria.class))).thenReturn(dummyMatchList);
		mvc.perform(post("/match/filter")
				.accept(MediaType.APPLICATION_JSON_VALUE)
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.content("{}"))
				.andExpect(status().isOk())
			    .andExpect(jsonPath("$", hasSize(2)));
    }
    
    @Test
    public void testGetMatchById() throws Exception {
    	Match dummyMatch = (Match) DummyObjectFactory.populateDummyObject(new Match());
		Long dummyId = new Long(101);
		dummyMatch.setId(dummyId);
		
		when(matchService.getMatchById(dummyMatch.getId())).thenReturn(Optional.ofNullable(dummyMatch));
		mvc.perform(get("/match/{id}", dummyMatch.getId()))
				.andExpect(status().isOk())
			    .andExpect(jsonPath("$.displayName", is(dummyMatch.getDisplayName())));
    }
    
    @Test
    public void testGetMatchById_notFound() throws Exception {
    	Match dummyMatch = (Match) DummyObjectFactory.populateDummyObject(new Match());
		Long dummyId = new Long(101);
		dummyMatch.setId(dummyId);
		
		when(matchService.getMatchById(dummyMatch.getId())).thenReturn(Optional.ofNullable(null));
		mvc.perform(get("/match/{id}", dummyMatch.getId()))
				.andExpect(status().isNotFound());
    }

}
