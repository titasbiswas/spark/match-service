package com.spark.match.repo;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.spark.match.model.City;
import com.spark.match.model.FilterCriteria;
import com.spark.match.model.LocationDistance;
import com.spark.match.model.Match;
import com.spark.match.model.Range;
import com.spark.match.util.JsonDbUtils;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations = "classpath:application-test.properties")
public class MatchRepositoryTest {

	@Autowired
	private MatchRepository matchRepository;

	@Autowired
	private JsonDbUtils jsonDbUtils;

	private List<Match> insertedTestData;
	private Match referenceData;

	@Before
	public void setupTestPrerequisites() {
		insertedTestData = jsonDbUtils.insertJsonToMatchTable("/json/testData.json");
		referenceData = Match.builder().age(38)
				.city(City.builder().lat(new BigDecimal("51.568535")).lon(new BigDecimal("-1.772232")).name("Swindon")
						.build())
				.compatibilityScore(new BigDecimal("0.88")).contactsExchanged(0).displayName("Anne").favourite(false)
				.heightInCm(170).jobTitle("Marketing Consultant")
				.mainPhoto("http://thecatapi.com/api/images/get?format=src&type=gif").religion("Jewish").build();
	}

	@Test
	public void testInsertion() {
		assertEquals(4, insertedTestData.size());
	}

	// 2 person should be within 100 KM
	@Test
	public void shouldBeWithinDistance() {
		FilterCriteria filterCriteria = FilterCriteria.builder()
				.locDist(LocationDistance.builder().lat(referenceData.getCity().getLat())
						.lon(referenceData.getCity().getLon()).distanceInKm(100.00).build())
				.build();
		List<Match> testData = matchRepository.getMatches(filterCriteria);
		testData.retainAll(insertedTestData);
		assertEquals(2, testData.size());
	}

	// 2 person should be Favourite
	@Test
	public void shouldBeFavourite() {
		FilterCriteria filterCriteria = FilterCriteria.builder().favourite(true).build();
		List<Match> testData = matchRepository.getMatches(filterCriteria);
		testData.retainAll(insertedTestData);
		assertEquals(2, testData.size());
	}

	// 3 person should have photo
	@Test
	public void shouldHavePhoto() {
		FilterCriteria filterCriteria = FilterCriteria.builder().hasPhoto(true).build();
		List<Match> testData = matchRepository.getMatches(filterCriteria);
		testData.retainAll(insertedTestData);
		assertEquals(3, testData.size());
	}

	// 3 person should be within 50% to 99% compatibility range
	@Test
	public void shouldBeWithinCompatibility() {
		FilterCriteria filterCriteria = FilterCriteria.builder()
				.compatibilityScore(Range.builder().min(50).max(99).build()).build();
		List<Match> testData = matchRepository.getMatches(filterCriteria);
		testData.retainAll(insertedTestData);
		assertEquals(3, testData.size());
	}

	// 2 person should be within 150cm to 180cm compatibility range
	@Test
	public void shouldBeWithinHeight() {
		FilterCriteria filterCriteria = FilterCriteria.builder().height(Range.builder().min(150).max(180).build())
				.build();
		List<Match> testData = matchRepository.getMatches(filterCriteria);
		testData.retainAll(insertedTestData);
		assertEquals(2, testData.size());
	}
	
	// 3 person should be within 35yr to 40yr compatibility range
		@Test
		public void shouldBeWithinAge() {
			FilterCriteria filterCriteria = FilterCriteria.builder().age(Range.builder().min(35).max(40).build())
					.build();
			List<Match> testData = matchRepository.getMatches(filterCriteria);
			testData.retainAll(insertedTestData);
			assertEquals(2, testData.size());
		}
		
		// 3 person should have contact exchanged
		@Test
		public void shouldBeInContact() {
			FilterCriteria filterCriteria = FilterCriteria.builder().inContact(true).build();
			List<Match> testData = matchRepository.getMatches(filterCriteria);
			testData.retainAll(insertedTestData);
			assertEquals(3, testData.size());
		}

	@After
	public void cleanUp() {

		matchRepository.deleteAll(insertedTestData);

	}

}
